SELECT CustomerID, CustomerName, ContactName FROM Customers WHERE Country = "USA"; 

CREATE VIEW usa_customers AS SELECT CustomerID, CustomerName, ContactName FROM Customers WHERE Country = "USA"; 

SELECT * FROM usa_customers;

SELECT * FROM usa_customers JOIN Orders ON usa_customers.CustomerID = Orders.CustomerID;

######################################################

SELECT avg(Price) FROM Products;

SELECT ProductID, ProductName, Price FROM Products WHERE Price < (SELECT avg(Price) FROM Products);

CREATE OR REPLACE VIEW products_below_avg_price AS SELECT ProductID, ProductName, Price FROM Products WHERE Price < 30;

######################################################

SELECT * FROM products_below_avg_price;

SELECT * FROM usa_customers JOIN Orders ON usa_customers.CustomerID = Orders.CustomerID WHERE OrderID IN (SELECT OrderID FROM OrderDetails JOIN products_below_avg_price ON OrderDetails.ProductID = products_below_avg_price.ProductID);

DROP VIEW usa_customers;