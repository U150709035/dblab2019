CALL selectAllCustomers;

CALL getCustomersByCity("Barcelona");

SELECT * FROM Employees;

###########################################

SET @max_salary = 0;

CALL highestSalary(@max_salary);

SELECT @max_salary;

###########################################

SET @m_count = 0;

CALL countGender(@m_count, "M");

SET @f_count = 0;

CALL countGender(@f_count, "F");

SELECT @m_count, @f_count;
