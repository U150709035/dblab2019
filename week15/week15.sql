SELECT * FROM denormalized;

INSERT INTO movies(movie_id, title, ranking, rating, year, votes, duration, oscars, budget) SELECT DISTINCT movie_id, title, ranking, rating, year, votes, duration, oscars, budget FROM denormalized;

INSERT INTO languages(movie_id, language_name) SELECT DISTINCT movie_id, language_name FROM denormalized;

INSERT INTO genres(movie_id, genre_name) SELECT DISTINCT movie_id, genre_name FROM denormalized;

INSERT INTO countries(country_id, country_name) SELECT DISTINCT producer_country_id, producer_country_name FROM denormalized UNION SELECT DISTINCT director_country_id, director_country_name FROM denormalized UNION SELECT DISTINCT star_country_id, star_country_name FROM denormalized;

INSERT INTO producer_countries(movie_id, country_id) SELECT DISTINCT movie_id, producer_country_id FROM denormalized;

INSERT INTO directors(director_id, country_id, director_name) SELECT DISTINCT director_id, director_country_id, director_name FROM denormalized;

INSERT INTO stars(star_id, country_id, star_name) SELECT DISTINCT star_id, star_country_id, star_name FROM denormalized;

INSERT INTO movie_stars(movie_id, star_id) SELECT DISTINCT movie_id, star_id FROM denormalized;

INSERT INTO movie_directors(movie_id, director_id) SELECT DISTINCT movie_id, director_id FROM denormalized;




