# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
SELECT title FROM movies WHERE budget > 10000000 AND ranking < 6; 
# 2. Show the action films whose rating is greater than 8.8 and produced after 1990.
SELECT movies.movie_id, movies.title FROM movies JOIN genres ON movies.movie_id = genres.movie_id WHERE rating > 8.8 AND year > 1990  AND genre_name = "action";
# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
SELECT movies.movie_id, movies.title FROM movies WHERE duration > 150 AND oscars > 2 AND movie_id in (SELECT movie_id FROM genres WHERE genre_name = "drama");
# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
SELECT title FROM movies WHERE oscars > 2 and movie_id in (SELECT movie_id FROM movie_stars JOIN stars ON movie_stars.star_id = stars.star_id WHERE star_name = "Orlando Bloom" AND movie_id in (SELECT movie_id FROM movie_stars JOIN stars ON movie_stars.star_id = stars.star_id WHERE star_name = "Ian McKellen"));
# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.	 
SELECT title FROM movies WHERE year < 2000 AND votes > 500000 AND movie_id IN (SELECT movie_id FROM movie_directors JOIN directors on directors.director_id = movie_directors.director_id WHERE directors.director_name LIKE "Quentin%");
# 6. Show the thriller films whose budget is greater than 25 million$.	 
SELECT movies.movie_id, title FROM movies JOIN genres on movies.movie_id = genres.movie_id WHERE budget > 25000000 AND genre_name = "Thriller";
# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
SELECT title FROM movies WHERE year BETWEEN 1990 AND 2000 AND movie_id IN (SELECT languages.movie_id FROM languages JOIN genres on languages.movie_id = genres.movie_id WHERE language_name = "Italian" AND genre_name = "Drama");
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.
SELECT title FROM movies WHERE oscars > 3 AND movie_id IN (SELECT movie_id FROM movie_stars JOIN stars ON movie_stars.star_id = stars.star_id WHERE star_name LIKE "%Hanks");
# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
SELECT title FROM movies WHERE duration BETWEEN 100 AND 200 AND movie_id in (SELECT genres.movie_id FROM (genres JOIN producer_countries ON genres.movie_id = producer_countries.movie_id) JOIN countries ON producer_countries.country_id = countries.country_id WHERE genre_name = "History" AND country_name = "USA");
# 10.Compute the average budget of the films directed by Peter Jackson.

# 11.Show the Francis Ford Coppola film that has the minimum budget.

# 12.Show the film that has the most vote and has been produced in USA.